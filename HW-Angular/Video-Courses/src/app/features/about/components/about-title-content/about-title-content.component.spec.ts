import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutTitleContentComponent } from './about-title-content.component';

describe('AboutTitleContentComponent', () => {
  let component: AboutTitleContentComponent;
  let fixture: ComponentFixture<AboutTitleContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AboutTitleContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutTitleContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
