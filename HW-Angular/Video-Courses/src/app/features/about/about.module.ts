import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './containers/about/about.component';
import { AboutRoutingModule } from './about-routing.module';
import { AboutTitleContentComponent } from './components/about-title-content/about-title-content.component';



@NgModule({
  declarations: [
    AboutComponent,
    AboutTitleContentComponent
  ],
  imports: [
    CommonModule,
    AboutRoutingModule
  ]
})
export class AboutModule { }
