import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { Page404Component } from "./core/components/page404/page404.component";

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'courses'
    },
    {
        path: 'courses',
        loadChildren: () => import('./features/courses/courses.module').then(
            (m) => m.CoursesModule
        ),
    },
    {
        path: 'about',
        loadChildren: () => import('./features/about/about.module').then(
            (m) => m.AboutModule
        ),
    },
    { path: '**', component: Page404Component },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}