At the first install node.js

Then download dependencies (npm install)

# Development server

npm run start (Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files).

# Build

npm run build (Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory).

# Running unit tests

npm run test (Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io)).