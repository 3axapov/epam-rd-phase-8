import Post from '../BlogWorld/js/Posts/Post.js';

import MelodyPost from '../BlogWorld/js/Posts/Melody.js';
import TextPost from "../BlogWorld/js/Posts/Text.js";
import VideoPost from "../BlogWorld/js/Posts/Video.js";
import PicturePost from "../BlogWorld/js/Posts/Picture.js";

describe('posts.js', () => {
    
    let mockPost, mockVideo, post;

    let section;

    beforeEach(() => {
        section = document.createElement('article');
        document.body.append(section);

        mockPost = {
            avatar: `/https://secure.gravatar.com/avatar/3593437cbd05cebe0a4ee753965a8ad1.jpg`,
            averageRating: 8.3,
            backdrop: `/yizL4cEKsVvl17Wc1mGEIrQtM2F.jpg`,
            content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                Sed auctor tempus ligula eu aliquet. Cras dignissim enim quis 
                pharetra molestie. Praesent ultricies consectetur lectus, ut 
                varius orci vulputate at. Vestibulum tortor magna, convallis 
                id mattis eget, luctus ut erat. Aenean ullamcorper dui ac lorem 
                scelerisque, vitae maximus eros imperdiet. Nulla facilisi. Aenean 
                eleifend arcu ut dolor molestie ultricies. Interdum et malesuada 
                fames ac ante ipsum primis in faucibus. Pellentesque scelerisque 
                arcu sodales varius dapibus. Vivamus suscipit, neque eu condimentum 
                pretium, ante libero volutpat leo, id venenatis massa nulla et nisl. 
                Mauris id tincidunt neque, eu congue ipsum. Phasellus sit amet.`,
            created: `2021-07-01T19:34:19.699Z`,
            filmTitle: `The Tomorrow War`,
            id: 588228,
            link: `https://www.themoviedb.org/review/60de18bbb686b9002d5d8e8c`,
            name: `garethmb`,
            originalTitle: `The Tomorrow War`,
            poster: `/34nDCQZwaEvsy4CFO5hkGRFDCVU.jpg`,
            release: `2021-06-30`,
            reviewCount: 3,
            starsRating: 5.4,
            type: `review`,
            updated: `2021-07-01T19:34:19.699Z`,
            username: `garethmb`,
        };

        mockVideo = {
            id: "5de6d4fc11386c001653ee95",
            iso_639_1: "en",
            iso_3166_1: "US",
            key: "RxAtuMu_ph4",
            name: "Official Teaser Trailer",
            site: "YouTube",
            size: 1080,
            type: "Trailer",
        }
    });

    afterEach(() => {
        section.parentNode.removeChild(section);
    });

    describe('Melody.js', () => {

        beforeEach(() => {
            post = new MelodyPost(mockPost, 120);
            section.innerHTML = post.printTemplate();    
        });

        it('should be instance of parent class Post', ()=> {
            expect(post).toBeInstanceOf(Post);
            expect(post).toBeInstanceOf(MelodyPost);
        });

        it('post has audio', ()=> {
            let audioTrack = section.getElementsByTagName('audio').length;
            expect(audioTrack).toBeTruthy();
        });
    });

    describe('Text.js', () => {

        beforeEach(() => {
            post = new TextPost(mockPost, 530);
            section.innerHTML = post.printTemplate();    
        });

        it('should be instance of parent class Post', ()=> {
            expect(post).toBeInstanceOf(Post);
            expect(post).toBeInstanceOf(TextPost);
        });
    });

    describe('Video.js', () => {

        beforeEach(() => {
            post = new VideoPost(mockPost, 200, mockVideo);
            section.innerHTML = post.printTemplate();    
        });

        it('should be instance of parent class Post', ()=> {
            expect(post).toBeInstanceOf(Post);
            expect(post).toBeInstanceOf(VideoPost);
        });

        it('post has video', ()=> {
            let video = section.querySelector('.video__player');
            expect(video).toBeTruthy();
        });
    });

    describe('Picture.js', () => {

        beforeEach(() => {
            post = new PicturePost(mockPost, 200);
            section.innerHTML = post.printTemplate();    
        });

        it('should be instance of parent class Post', ()=> {
            expect(post).toBeInstanceOf(Post);
            expect(post).toBeInstanceOf(PicturePost);
        });

        it('post has picture', ()=> {
            let picture = section.querySelector('.picture__preview');
            expect(picture).toBeTruthy();
        });
    });

    describe('Post.js', () => {
        let letters = 103;

        beforeEach(() => {
            post = new TextPost(mockPost, letters);
            section.innerHTML = post.printTemplate();    
        });

        it('should trim release date to year', ()=> {
            expect(post._release).toBe(2021);
        });

        it('should calculate reading time', ()=> {
            let readTimeSpy = spyOn(post, 'getReadTime');

            post.printTemplate();

            expect(readTimeSpy).toHaveBeenCalled();
            expect(section.querySelector('.stats__read-time').textContent).toEqual('9');
        });

        it('should calculate reading time', ()=> {        
            let anotherPost = new TextPost(mockPost, letters);
            anotherPost._content = '';

            section.innerHTML = anotherPost.printTemplate();    

            let readTimeSpy = spyOn(anotherPost, 'getReadTime');

            anotherPost.printTemplate();
            expect(readTimeSpy).toHaveBeenCalled();
            expect(section.querySelector('.stats__read-time').textContent).toEqual('1');
        });

        it('should format post dates', ()=> {
            let formatDate = spyOn(Post, 'formatDate');

            post.printTemplate();

            expect(formatDate).toHaveBeenCalledTimes(2);
            expect(section.querySelector('.stats__date').textContent).toEqual('1 jul, 2021');
        });

        it('should convert rating in stars', ()=> {
            let starsSpy = spyOn(post, 'getRating');
            let stars = section.querySelector('.text__stars').children;

            post.printTemplate();

            expect(starsSpy).toHaveBeenCalled();
            
            expect(stars[0].alt === 'star').toBeTrue();
            expect(stars[1].alt === 'star').toBeTrue();
            expect(stars[2].alt === 'half star').toBeTrue();
            expect(stars[3].alt === 'empty star').toBeTrue();
            expect(stars[4].alt === 'empty star').toBeTrue();
        });

        it('should trim post`s text', ()=> {
            let formatTextSpy = spyOn(post, 'formatText');
            let regex = new RegExp('.{1,' + letters + '}\.{3}$');

            post.printTemplate();

            expect(formatTextSpy).toHaveBeenCalled();
            expect(section.querySelector('.text__paragraph').textContent).toMatch(regex);
        });

        it('should set default avatar', ()=> {
            let anotherPost = new TextPost(mockPost, letters);
            anotherPost._avatar = null;

            section.innerHTML = anotherPost.printTemplate();    

            let setAvatarSpy = spyOn(anotherPost, 'setAvatar');

            anotherPost.printTemplate();
            expect(setAvatarSpy).toHaveBeenCalled();
            expect(section.querySelector('.user__photo-img').src).toMatch('/noavatar.png')
        });

        it('should set another avatar path', ()=> {
            let anotherPost = new TextPost(mockPost, letters);
            anotherPost._avatar = 'photo';

            section.innerHTML = anotherPost.printTemplate();    

            let setAvatarSpy = spyOn(anotherPost, 'setAvatar');

            anotherPost.printTemplate();
            expect(setAvatarSpy).toHaveBeenCalled();
            expect(section.querySelector('.user__photo-img').src).toMatch('/w64_and_h64_face')
        });
    });
});