import { SliderBase, CaruselSlider, ScaleSlider } from '../BlogWorld/js/slider.js';

describe('slider.js', () => {

    let container, track, nextBtn, prevBtn, slides;

    let slider;
        
    beforeEach(() => {
        container = document.createElement('div');
        track = document.createElement('div');
        nextBtn = document.createElement('div');
        prevBtn = document.createElement('div');
        
        let slide1 = document.createElement('div');
        slide1.classList.add('slider-item');
        
        let slide2 = document.createElement('div');
        slide2.classList.add('slider-item');

        document.body.append(container, nextBtn, prevBtn);            
        container.append(track);
        track.append(slide1, slide2);

        slides = document.querySelectorAll('.slider-item');
    });

    afterEach(() => {
        container.parentNode.removeChild(container);
    });

    describe('SliderBase()', () => {

        beforeEach(() => {
            slider = new ScaleSlider(container, track, nextBtn, prevBtn, slides);
        });

        const triggerEvent = (elem, e) => {
            const event = new Event(e);
            elem.dispatchEvent(event);
        }
        
        it('should change slide forward', () => {
            let forwardSpy = spyOn(slider, 'moveForward');
            nextBtn.click();
            expect(forwardSpy).toHaveBeenCalled();
        });

        it('should disable button "next"', () => {
            jasmine.clock().install();
            nextBtn.click();
            jasmine.clock().tick(1200);
            expect(nextBtn.getAttribute('disabled')).toBe('disabled');
            jasmine.clock().tick(1400);
            expect(nextBtn.getAttribute('disabled')).toBe(null);
            
            jasmine.clock().uninstall()
        });

        it('should change slide backward', () => {
            let backwardSpy = spyOn(slider, 'moveBackward');
            prevBtn.click();
            expect(backwardSpy).toHaveBeenCalled();
        });

        it('should disable button "back"', () => {
            jasmine.clock().install();
            prevBtn.click();
            jasmine.clock().tick(1200);
            expect(prevBtn.getAttribute('disabled')).toBe('disabled');
            jasmine.clock().tick(1400);
            expect(prevBtn.getAttribute('disabled')).toBe(null);
            
            jasmine.clock().uninstall()
        });

        it('should return 2 slides', () => {
            expect(slider.getSlides('.slider-item').length).toBeNumber();
            expect(slider.getSlides('.slider-item').length/3).toBe(2);
        });

        it('should slide backward', () => {
            slider.index = 1;
            slider.moveBackward();
            expect(slider.index).toBe(0);
        });

        it('dont out of bounds (< 0)', () => {
            slider.index = 0;
            slider.moveBackward();
            expect(slider.index).toBe(0);
        });

        it('should slide forward', () => {
            slider.index = 0;
            slider.moveForward();
            expect(slider.index).toBe(1);
        });

        it('dont out of bounds (> array of slides)', () => {
            slider.index = slider.slides.length;
            slider.moveForward();
            expect(slider.index).toBe(slider.slides.length);
        });

        it('should react on transition event 1', () => {
            let transitionSpy = spyOn(slider, 'trackTransitionend');
            
            triggerEvent(track, 'transitionend');

            expect(transitionSpy).toHaveBeenCalledOnceWith(slider.slides, slider.track, slider.index);
        });

        it('should react on transition event 2', () => {
            slider.index = 0;
            let transitionSpy = spyOn(slider, 'trackTransitionend');
            
            triggerEvent(track, 'transitionend');

            expect(transitionSpy).toHaveBeenCalledOnceWith(slider.slides, slider.track, slider.index);
        });

        it('should react on transition event 3', () => {
            slider.index = 4;
            let transitionSpy = spyOn(slider, 'trackTransitionend');
            
            triggerEvent(track, 'transitionend');

            expect(transitionSpy).toHaveBeenCalledOnceWith(slider.slides, slider.track, slider.index);
        });

        it('should stop animation', () => {
            let spy = spyOn(global, 'clearInterval');

            triggerEvent(nextBtn, 'mouseenter');
            
            expect(spy).toHaveBeenCalledWith(slider.slideId);
        });
    });

    describe('ScaleSlider()', () => {

        beforeEach(() => {
            slider = new ScaleSlider(container, track, nextBtn, prevBtn, slides);
        });

        it('should be scale slider', () => {
            expect(slider instanceof ScaleSlider).toBeTruthy();
            expect(slider instanceof SliderBase).toBeTruthy();
            expect(slider.type).toBe('scale');
        });

        it('should be different sliders', () => {
            let anotherSlider = new ScaleSlider(container, track, nextBtn, prevBtn, slides);
            expect(slider).not.toEqual(anotherSlider);
        });

        it('should NOT be created', () => {
            let anotherSlider = new ScaleSlider(container, track, nextBtn, prevBtn, null);
            expect(anotherSlider.type).toBeUndefined();
        });

        it('should change animation', () => {
            slider.animation(slides, track, slider.index);
            expect(slides[0].style.transform).toEqual('scale(0)');
        });

        it('should change track transition', () => {
            slider.trackTransitionend(slides, track, slider.index);
            expect(track.style.transition).toMatch('');
        });
    });

    describe('CaruselSlider()', () => {

        beforeEach(() => {
            slider = new CaruselSlider(container, track, nextBtn, prevBtn, slides);
        });

        it('should be carusel slider', () => {
            expect(slider instanceof CaruselSlider).toBeTruthy();
            expect(slider instanceof SliderBase).toBeTruthy();
            expect(slider.type).toBe('carusel');
        });

        it('should be different sliders', () => {
            let anotherSlider = new CaruselSlider(container, track, nextBtn, prevBtn, slides);
            expect(slider).not.toEqual(anotherSlider);
        });

        it('should NOT be created', () => {
            let anotherSlider = new CaruselSlider(container, track, nextBtn, prevBtn, null);
            expect(anotherSlider.type).toBeUndefined();
        });

        it('should add track transition', () => {
            slider.animation(slides, track, slider.index);
            expect(track.style.transition).toMatch('1.4s ease-out');
        });

        it('should remove track transition', () => {
            slider.trackTransitionend(slides, track, slider.index);
            expect(track.style.transition).toMatch('');
        });
    });
});