module.exports = function (config) {
    config.set({
        frameworks: ['jasmine', 'jasmine-matchers'],
        preprocessors: {
            'test/*.spec.js': ['webpack'],
            'BlogWorld/js/**/*.js': ['webpack', 'coverage'],
        },
        files: [
            {pattern: 'test/*.spec.js', watched: true}
        ],
        plugins: [
            'karma-jasmine',
            'karma-jasmine-matchers',
            'karma-jasmine-html-reporter',
            'karma-spec-reporter',
            'karma-chrome-launcher',
            'karma-coverage',
            'karma-webpack'
        ],
        reporters: ['spec','kjhtml','coverage'],
        colors: true,
        browsers: ['Chrome'],
        singleRun: false,
        autoWatch: true,
        concurrency: Infinity,
        client:{
            clearContext: false
        },
        coverageReporter: {
            dir: './coverage/',
            reporters: [
                {type: 'html', subdir: 'html'}
            ],
        },
        webpack: {
            module: {
                rules: [
                    {
                        test: /\.(png|svg)$/i,
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                        },
                    },
                    {
                        test: /\.js/,
                        include: /BlogWorld/,
                        exclude: /node_modules|\.spec\.js$/,
                        use: "@jsdevtools/coverage-istanbul-loader"
                    },
                ]
            }
        }
    });
};
