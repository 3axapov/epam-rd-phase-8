import jQuery from 'jquery';

import successIcon from '../img/done-tick.svg';
import failIcon from '../img/error-tick.svg';
import confirmIcon from '../img/confirm-tick.svg';
import infoIcon from '../img/info-tick.svg';

(function($) {
    const ESCAPE_KEY_ID = 27;
    $.fn.modal = function(options) {
        let modal = {
            settings: {
                type: options.type || 'Error',
                message: options.message || 'Something went wrong.',
                name: options.name || 'Window',
                overlayColor: options.color || 'black',
                event: options.event || 'click',
                delay: options.delay || 0
            },

            init: link => {
                link.on(modal.settings.event, () => {
                    setTimeout(() => {
                        modal.create(modal.settings.type, modal.settings.overlayColor);
                    }, modal.settings.delay);
                });
            },

            create: (type, color) => {
                let overlay = $('<div class="my-modal__overlay"></div>').css('background-color', color);
                let modalWindow = $('<div></div>').addClass('my-modal');
                
                let headerName = $(`<div>${modal.settings.name}</div>`).addClass('my-modal__header');
                let crosshair = $('<button title="Close"></button>').addClass('my-modal__close');

                let okBtn = $('<button title="Ok"></button>').addClass('my-modal__ok button button-primary').text('Ok');

                let header = $('<div></div>').addClass('my-modal__header');
                let main = $('<div></div>').addClass('my-modal__main');
                let footer = $('<div></div>').addClass('my-modal__footer');

                $(modalWindow).append(header.append(headerName, crosshair), main, footer.append(okBtn));
                $('main').append(modalWindow, overlay);
                
                okBtn.on('click', function() {
                    modal.remove(modalWindow, overlay);
                });

                crosshair.on('click', function() {
                    modal.remove(modalWindow, overlay);
                });

                overlay.on('click', function() {
                    modal.remove(modalWindow, overlay);
                });

                $(window).keyup(function(e) {
                    if (e.keyCode === ESCAPE_KEY_ID) {
                        modal.remove(modalWindow, overlay);
                    }
                });

                modal.fill(type, modalWindow, overlay, main, footer, okBtn);
                
                return modalWindow;
            },

            fill: (type, modalWindow, overlay, main, footer, okBtn) => {
                let cancelBtn, input;
                let img = $('<img />');
                let imgBox = $('<div></div>').addClass('my-modal__img');
                let text = $('<div></div>').addClass('my-modal__message').text(modal.settings.message);
                
                switch (type.toLowerCase()) {
                case 'confirm':
                    img.attr('src', confirmIcon);

                    cancelBtn = $('<button title="Cancel"></button>')
                        .addClass('my-modal__cancel button button-primary').text('Cancel');

                    okBtn.modal({type:'Success', message:'Done!', name:'Success'});
                    cancelBtn.modal({type:'Error'});

                    cancelBtn.on('click', function() {
                        modal.remove(modalWindow, overlay);
                    });

                    footer.append(cancelBtn);
                    break;
                case 'error':
                    img.attr('src', failIcon);
                    break;
                case 'success':
                    img.attr('src', successIcon);
                    break;
                case 'input':
                    input = $('<input />').attr({
                        type: 'email',
                        placeholder: 'Your Email',
                        id: 'subscribeInput'
                    });

                    okBtn.attr('disabled', 'disabled');

                    input.on('input', function() {
                        if (/^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/.test(input.val())){
                            okBtn.removeAttr('disabled');
                        }
                    });

                    text.append(input);
                    img.attr('src', infoIcon);
                    
                    cancelBtn = $('<button title="Cancel"></button>')
                        .addClass('my-modal__cancel button button-primary').text('Cancel');

                    okBtn.modal({type:'Success', text:'Done!', name:'Success'});

                    cancelBtn.on('click', function() {
                        modal.remove(modalWindow, overlay);
                    });
                        
                    footer.append(cancelBtn);
                    break;
                default:
                    break;
                }

                main.append(imgBox.append(img), text);
            },

            remove: (modal, overlay) => {
                modal.remove();
                overlay.remove();
            }
        };

        return this.each(function() {
            let link = $(this);
            modal.init(link);
        });
    };
})(jQuery);