import Post from './Post.js';

import commentIcon from '../../img/a-icon-comment.svg';
import typeIcon from '../../img/a-icon-melody.svg';

export default class MelodyPost extends Post {

    constructor(options, letters) {
        super(options);
        this.letters = letters;
    }

    printTemplate() {
        return (
            `<div class="melody__sign">
                <div>
                    <img src="${typeIcon}" alt="">
                </div>
            </div>
        
            <div class="melody__preview">
                <img 
                    src="https://image.tmdb.org/t/p/w533_and_h300_bestv2${this._backdrop}" 
                    class="melody__preview-img"  
                    alt="backdrop" />
            </div>
        
            <div class="melody__content">
                <div class="melody__info user">
                    <div class="user__photo">
                        <img 
                            src="${this.setAvatar()}" 
                            alt="Authors Photo" 
                            class="user__photo-img"/>
                    </div>
                    <div>
                        <p class="user__name" title="${this._username}">
                            ${this._name}
                        </p>
                        <p class="melody__stats">
                            <span title="created: ${Post.formatDate(this._created)}">
                                <span class="stats__date">${Post.formatDate(this._updated)}</span> &nbsp;&nbsp;&bull; &nbsp;
                            </span>
                            <span>
                                <span class="stats__read-time">${this.getReadTime()}</span> min read &nbsp;&nbsp;&bull; &nbsp;
                            </span>
                            <span>
                                <img 
                                    src="${commentIcon}" 
                                    alt=""
                                    class="post__comments"/>&nbsp;
                                ${this._reviewCount}
                            </span>
                            <span 
                                class="melody__stars" 
                                title="average: ${this._averageRating}/10">
                                ${this.getRating()}
                            </span>
                        </p>
                    </div>
                </div>
        
                <div class="melody__text">
                    <h3 
                        class="melody__title" 
                        title="Original: ${this._originalTitle}">
                        ${this._filmTitle} (${this._release})
                    </h3>
                    <audio 
                        controls 
                        src="#" 
                        class="melody__audio">
                        Your browser does not support the <code>audio</code> element.
                    </audio>
                    <p class="melody__paragraph">${this.formatText(this.letters)}</p>
                    <button 
                        onclick="window.open('${this._link}');" 
                        class="button button-secondary">
                        Read more
                    </button>
                    <button  
                        class="open-modal button button-secondary">
                        Delete Post
                    </button>
                </div>
            </div>`
        );
    }
}