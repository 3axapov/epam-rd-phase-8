import Post from './Post.js';

import commentIcon from '../../img/a-icon-comment.svg';
import typeIcon from '../../img/a-icon-text.svg';

export default class TextPost extends Post {

    constructor(options, letters) {
        super(options);
        this.letters = letters;
    }

    printTemplate() {
        return ( 
            `<div class="text__sign">
                <div>
                    <img src="${typeIcon}" alt="">
                </div>
            </div>
        
            <div class="text__content">
                <div class="text__info user">
                    <div class="user__photo">
                        <img 
                            src="${this.setAvatar()}" 
                            alt="Authors Photo" 
                            class="user__photo-img"/>
                    </div>
                    <div>
                        <p class="user__name" title="${this._username}">
                            ${this._name}
                        </p>
                        <p class="text__stats">
                            <span title="created: ${Post.formatDate(this._created)}">
                                <span class="stats__date">${Post.formatDate(this._updated)}</span> &nbsp;&nbsp;&bull; &nbsp;
                            </span>
                            <span>
                                <span class="stats__read-time">${this.getReadTime()}</span> min read &nbsp;&nbsp;&bull; &nbsp;
                            </span>
                            <span>
                                <img 
                                    src="${commentIcon}" 
                                    alt=""
                                    class="post__comments">&nbsp;
                                ${this._reviewCount}
                            </span>
                            <span 
                                class="text__stars" 
                                title="average: ${this._averageRating}/10">
                                ${this.getRating()}
                            </span>
                        </p>
                    </div>
                </div>
        
                <div class="text__text">
                    <h3 
                        class="text__title" 
                        title="Original: ${this._originalTitle}">
                        ${this._filmTitle} (${this._release})
                    </h3>
                    <p class="text__paragraph">${this.formatText(this.letters)}</p>
                    <button 
                        onclick="window.open('${this._link}');" 
                        class="button button-secondary">
                        Read more
                    </button>
                    <button  
                        class="open-modal button button-secondary">
                        Delete Post
                    </button>
                </div>
            </div>`
        );
    }
}