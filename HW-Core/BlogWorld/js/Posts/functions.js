import MelodyPost from './Melody.js';
import TextPost from './Text.js';
import VideoPost from './Video.js';
import PicturePost from './Picture.js';

import { getData } from '../useApi.js';

export default async function createArticle(film, position, letters, postType, videos) {
    if (film === undefined) {
        return position.innerHTML = '404 film not found';
    }

    let post;

    if (postType === PicturePost || postType === MelodyPost) {
        if (film.backdrop_path === null) {
            return createArticle(film, textArticle, 530, TextPost);
        }
    }

    if (postType === VideoPost) {
        if (videos.length === 0) {
            return createArticle(film, pictureArticle, 200, PicturePost);
        }
        
        let video = videos[0];

        post = new postType(await getData(film), letters, video);
    } else {
        post = new postType(await getData(film), letters);
    }

    return position.innerHTML = post.printTemplate();
}