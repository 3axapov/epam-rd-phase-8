import Post from './Post.js';

import commentIcon from '../../img/a-icon-comment.svg';
import typeIcon from '../../img/a-icon-playmini.svg';
import playIcon from '../../img/a-icon-play.svg';

export default class VideoPost extends Post {

    constructor(options, letters, video) {
        super(options);
        this.letters = letters;
        this.video = video;
    }

    printTemplate() {
        return (
            `<div class="video__sign">
                <div>
                    <img src="${typeIcon}" alt="">
                </div>
            </div>

            <div class="video__preview" onclick="console.log('click')">
                <iframe
                    src="https://www.youtube.com/embed/${this.video.key}?controls=0" 
                    class="video__player"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture">
                </iframe>
                <img src='${playIcon}' alt="Play" class="video__play" id="playBtn">
            </div>

            <div class="video__content">
                <div class="video__info user">
                    <div class="user__photo">
                        <img 
                            src="${this.setAvatar()}" 
                            alt="Authors Photo" 
                            class="user__photo-img"/>
                    </div>
                    <div>
                        <p class="user__name" title="${this._username}">
                            ${this._name}
                        </p>
                        <p class="video__stats">
                            <span title="created: ${Post.formatDate(this._created)}">
                                <span class="stats__date">${Post.formatDate(this._updated)}</span> &nbsp;&nbsp;&bull; &nbsp;
                            </span>
                            <span>
                                <span class="stats__read-time">${this.getReadTime()}</span> min read &nbsp;&nbsp;&bull; &nbsp;
                            </span>
                            <span>
                                <img 
                                    src="${commentIcon}" 
                                    alt=""
                                    class="post__comments"/>&nbsp;
                                ${this._reviewCount}
                            </span>
                            <span 
                                class="video__stars" 
                                title="average: ${this._averageRating}/10">
                                ${this.getRating()}
                            </span>
                        </p>
                    </div>
                </div>

                <div class="video__text">
                    <h3 
                        class="video__title" 
                        title="Original: ${this._originalTitle}">
                        ${this._filmTitle} (${this._release})
                    </h3>
                    <p class="video__paragraph">${this.formatText(this.letters)}</p>
                    <button 
                        onclick="window.open('${this._link}');" 
                        class="button button-secondary">
                        Read more
                    </button>
                    <button
                        class="open-modal button button-secondary">
                        Delete Post
                    </button>
                </div>
            </div>`
        );
    }
}