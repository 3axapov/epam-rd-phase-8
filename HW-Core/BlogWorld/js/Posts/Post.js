import fullStar from '../../img/Star-1.svg';
import halfStar from '../../img/Group.svg';
import emptyStar from '../../img/Star-2.svg';
import incognito from '../../img/noavatar.png';

export default class Post {

    constructor(options) {
        this._avatar = options.avatar;
        this._username = options.username ;
        this._name = options.name.split(':')[0] ;
        this._created = options.created ;
        this._updated = options.updated ;
        this._content = options.content ;
        this._starsRating = options.starsRating ;
        this._link = options.link ;
        this._id = options.id ;
        this._filmTitle = options.filmTitle ;
        this._originalTitle = options.originalTitle ;
        this._release = new Date(options.release).getFullYear();
        this._reviewCount = options.reviewCount;
        this._averageRating = options.averageRating;
        this._backdrop = options.backdrop;
    }

    setAvatar() {
        let avatar = this._avatar;
        return !avatar
            ? incognito
            : avatar.length > 32
                ? `https://secure.gravatar.com/avatar/${avatar.slice(
                    -36,
                    avatar.length
                )}?s=64`
                : `https://image.tmdb.org/t/p/w64_and_h64_face${avatar}`;
    }

    getReadTime() {
        const WORDS_PER_MINUTE = 100;
        let readTime = Math.round(this._content.length / WORDS_PER_MINUTE);
    
        return readTime < 1 ? 1 : readTime;
    }

    getRating() {
        let str = '';
        for (let i = 1; i <= 10; i++) {
            i < Math.floor(this._starsRating)
                ? (str += `<img src="${fullStar}" alt="star">`)
                : i === Math.floor(this._starsRating)
                    ? (str += `<img src="${halfStar}" alt="half star">`)
                    : (str += `<img src="${emptyStar}" alt="empty star">`);
    
            i++;
        }
        return str;
    }

    formatText(letters) {
        if (this._content.length > letters && letters !== undefined) {
            let newText = this._content.substring(0, letters).split(' ');
            return newText.splice(0, newText.length-1).join(' ') + '...';
        }
        return this._content;
    }

    static formatDate(date) {
        const MONTHS = [
            'jan',
            'fab',
            'mar',
            'apr',
            'may',
            'jun',
            'jul',
            'aug',
            'sep',
            'oct',
            'nov',
            'dec',
        ];
        let someDate = new Date(date);
        let month = someDate.getMonth();
    
        return `${someDate.getDate()} ${MONTHS[month]}, ${someDate.getFullYear()}`;
    }
}