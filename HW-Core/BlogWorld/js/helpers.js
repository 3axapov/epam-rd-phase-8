import '../scss/style.scss';
import 'core-js';
import 'regenerator-runtime/runtime';

const showpassBtn = document.getElementById('showpass');

const togglePasswordField = () => {
    const passwordInput = document.getElementById('password');

    if (passwordInput.type === 'password') {
        passwordInput.type = 'text';
        showpassBtn.innerHTML = '<img src="./img/assets/a-icon-hidepass.svg" alt="" /> Hide';
    } else {
        passwordInput.type = 'password';
        showpassBtn.innerHTML = '<img src="./img/assets/a-icon-showpass.svg" alt="" /> show';
    }
};

if (showpassBtn !== null) {
    showpassBtn.addEventListener('click', togglePasswordField);
}