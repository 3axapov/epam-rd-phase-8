import { CaruselSlider, ScaleSlider } from './slider.js';

const container_carusel = document.querySelector('.carusel-slider-container');
const track_carusel = document.querySelector('.carusel-slider-track');
const nextBtn_carusel = document.querySelector('.carusel-btn-next');
const prevBtn_carusel = document.querySelector('.carusel-btn-prev');
let items_carusel = document.querySelectorAll('.carusel-slider-item');

const container_scale = document.querySelector('.scale-slider-container');
const track_scale = document.querySelector('.scale-slider-track');
const nextBtn_scale = document.querySelector('.scale-btn-next');
const prevBtn_scale = document.querySelector('.scale-btn-prev');
let items_scale = document.querySelectorAll('.scale-slider-item');


new CaruselSlider(container_carusel, track_carusel, nextBtn_carusel, prevBtn_carusel, items_carusel);

new ScaleSlider(container_scale, track_scale, nextBtn_scale, prevBtn_scale, items_scale);