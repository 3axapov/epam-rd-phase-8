const API_URL = 'https://api.themoviedb.org/3/';
const API_KEY = 'api_key=dc9a2dc08c5b1f0abac0c6deacfde7cb';

const getFilms = async () => {
    let res = await fetch(`${API_URL}movie/popular?${API_KEY}`);
    let { results } = await res.json();

    return results;
};

const getInfo = async (id, category = 'reviews') => {
    let res = await fetch(`${API_URL}movie/${id}/${category}?${API_KEY}`);
    let { results } = await res.json();

    return results;
};

const getData = async (film) => {
    const filmReviews = await getInfo(film.id, 'reviews');
    const lastReview = filmReviews[0];

    let data = {
        avatar: film.poster_path,
        poster: film.poster_path,
        username: film.original_title,
        name: film.title,
        created: film.release_date,
        updated: film.release_date,
        content: film.overview,
        starsRating: film.vote_average,
        link: `https://www.themoviedb.org/movie/${film.id}`,
        id: film.id,
        filmTitle: film.title,
        originalTitle: film.original_title,
        release: film.release_date,
        reviewCount: filmReviews.length,
        averageRating: film.vote_average,
        backdrop: film.backdrop_path,
        type: 'film',
    };

    if (filmReviews.length !== 0) {
        data.avatar = lastReview.author_details.avatar_path;
        data.username = lastReview.author_details.username;
        data.name = lastReview.author;
        data.created = lastReview.created_at;
        data.updated = lastReview.updated_at;
        data.content = lastReview.content;
        data.starsRating = lastReview.author_details.rating;
        data.link = lastReview.url;
        data.type = 'review';
    }

    return data;
};

export { getFilms, getInfo, getData };