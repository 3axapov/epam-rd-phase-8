function SliderBase(slider) {
    this.slideId = 0;

    this.type = slider.type,
    this.index = slider.index,
    this.interval = slider.interval,
    this.slideWidth = slider.slideWidth,
    
    this.track = slider.track,
    this.container = slider.container,
    this.slides = slider.slides,
    this.nextBtn = slider.nextBtn,
    this.prevBtn = slider.prevBtn,

    this.animation = slider.animation,
    this.trackTransitionend = slider.trackTransitionend,

    this.move = function (track, slideWidth, index) {
        track.style.transform = `translateX(${-slideWidth * index}px)`;
    };

    this.startSlide = () => {
        this.slideId = setInterval(this.moveForward, this.interval);
    };

    this.getSlides = (selector) => document.querySelectorAll(selector);

    this.track.addEventListener('transitionend', () => {
        if (this.slides[this.index].dataset.id === 'first-clone') {
            this.index = this.track.children.length / 3;
        }

        if (this.slides[this.index].dataset.id === 'start-clone') {
            this.index = this.slides.length - this.track.children.length / 3;
        }
        this.trackTransitionend(this.slides, this.track, this.index);
    });

    this.moveBackward = () => {
        if (this.index <= 0) {
            return;
        }
        this.index--;
        this.slides = this.getSlides(`.${this.type}-slider-item`);
        this.animation(this.slides, this.track, this.index);
    };

    this.moveForward = () => {
        if (this.index >= this.slides.length - 1) {
            return;
        }
        this.index++;
        this.slides = this.getSlides(`.${this.type}-slider-item`);
        this.animation(this.slides, this.track, this.index);
    };

    this.stopAnimation = () => {
        clearInterval(this.slideId);
    };

    this.container.addEventListener('mouseenter', this.stopAnimation);
    this.container.addEventListener('mouseleave', this.startSlide);

    this.nextBtn.addEventListener('click', () => {
        this.moveForward();
        this.nextBtn.setAttribute('disabled', 'disabled');

        setTimeout(() => {
            this.nextBtn.removeAttribute('disabled');
        }, 1400);
    });
  
    this.nextBtn.addEventListener('mouseenter', this.stopAnimation);
    this.nextBtn.addEventListener('mouseleave', this.startSlide);

    this.prevBtn.addEventListener('click', () => {
        this.moveBackward();
        this.prevBtn.setAttribute('disabled', 'disabled');

        setTimeout(() => {
            this.prevBtn.removeAttribute('disabled');
        }, 1400);
    });

    this.prevBtn.addEventListener('mouseenter', this.stopAnimation);
    this.prevBtn.addEventListener('mouseleave', this.startSlide);

    this.move(this.track, this.slideWidth, this.index);
    this.startSlide();
}

function ScaleSlider(container, track, nextBtn, prevBtn, slides) {

    if (track===null || container===null || track===null || nextBtn===null || prevBtn===null || slides===null) {
        return 0;
    }

    this.type = 'scale';
    this.interval = 8000;
    
    slides.forEach((elem, i) => {
        let clone = elem.cloneNode(true);
        let cloneRev = elem.cloneNode(true);
        if (i === 0) {
            clone.dataset.id = 'first-clone';
            cloneRev.dataset.id = 'start-clone';
        }

        track.append(clone);
        track.insertBefore(cloneRev, slides[0]);
    });

    this.slides = track.children;

    this.index = track.children.length / 3;
    this.slideWidth = this.slides[0].clientWidth;

    const animation = (slides, track, index) => {
        slides.forEach((el) => {
            el.style.transition = '0.7s';
            el.style.transform = 'scale(0)';
        });

        setTimeout(() => {
            this.move(track, this.slideWidth, index);
        }, 700);
    };

    const trackTransitionend = (slides, track, index) => {
        slides.forEach((el) => {
            el.style.transition = '0.7s';
            el.style.transform = 'scale(1)';
        });

        track.style.transition = 'none';
        this.move(track, this.slideWidth, index);
    };

    SliderBase.call(this, {
        type: this.type,
        index: this.index,
        interval: this.interval,
        slideWidth: this.slideWidth,
        
        track: track,
        container: container,
        slides: this.slides,
        nextBtn: nextBtn,
        prevBtn: prevBtn,

        animation: animation,
        trackTransitionend: trackTransitionend,
    });
}

function CaruselSlider(container, track, nextBtn, prevBtn, slides) {

    if (track===null || container===null || track===null || nextBtn===null || prevBtn===null || slides===null) {
        return 0;
    }

    this.type = 'carusel';
    this.interval = 4000;

    slides.forEach((elem, i) => {
        let clone = elem.cloneNode(true);
        let cloneRev = elem.cloneNode(true);
        if (i === 0) {
            clone.dataset.id = 'first-clone';
            cloneRev.dataset.id = 'start-clone';
        }

        track.append(clone);
        track.insertBefore(cloneRev, slides[0]);
    });

    this.slides = track.children;
    this.index = track.children.length / 3;
    this.slideWidth = this.slides[0].clientWidth;

    const animation = (slides, track, index) => {
        track.style.transition = '1.4s ease-out';
        this.move(track, this.slideWidth, index);
    };

    const trackTransitionend = (slides, track, index) => {
        track.style.transition = 'none';
        this.move(track, this.slideWidth, index);
    };

    SliderBase.call(this, {
        type: this.type,
        index: this.index,
        interval: this.interval,
        slideWidth: this.slideWidth,
        
        track: track,
        container: container,
        slides: this.slides,
        nextBtn: nextBtn,
        prevBtn: prevBtn,

        animation: animation,
        trackTransitionend: trackTransitionend,
    });
}

ScaleSlider.prototype = Object.create(SliderBase.prototype);
CaruselSlider.prototype = Object.create(SliderBase.prototype);

export { ScaleSlider, SliderBase, CaruselSlider };