import createArticle from './Posts/functions.js';
import { getFilms, getInfo, getData } from './useApi.js';

import $ from 'jquery';
import incognito from '../img/noavatar.png';
import TextPost from './Posts/Text.js';
import PicturePost from './Posts/Picture.js';
import MelodyPost from './Posts/Melody.js';
import VideoPost from './Posts/Video.js';

const postsCollection = document.getElementsByTagName('article');
const videoArticle = document.getElementById('video');
const textArticle = document.getElementById('text');
const melodyArticle = document.getElementById('melody');
const pictureArticle = document.getElementById('picture');

const searchInput = document.getElementById('search');
const searchSwitcher = document.getElementById('switcher');
const searchResults = document.getElementById('searchResults');

window.onload = function() {
    let switcherStatus = (localStorage.getItem('switcher') === 'true');
    searchSwitcher.checked = switcherStatus;
    searchInput.value = localStorage.getItem('filter');
};

$('article').first().on('DOMSubtreeModified', () => {
    if ($('article').first().text() !== '') {
        $('.open-modal').modal({
            type:'Confirm', 
            message:'Are you sure?', 
            name:'Confirm'
        });
    }
});

$(window).modal({
    type:'Input', 
    message:'Subscribe to this blog and get new updates first.\n', 
    name:'Subscribe', 
    color:'white', 
    event:'load', 
    delay:10000
});

$(window).trigger('customLoad');

const createPosts = async (elem, i, filmsArr) => {
    const film = filmsArr[i];

    if (elem.classList.contains('video')) {
        createArticle(film, videoArticle, 200, VideoPost, await getInfo(film.id, 'videos'));
    }

    if (elem.classList.contains('melody')) {
        createArticle(film, melodyArticle, 110, MelodyPost);
    }
    
    if (elem.classList.contains('picture')) {
        createArticle(film, pictureArticle, 200, PicturePost);
    }

    if (elem.classList.contains('text')) {
        createArticle(film, textArticle, 530, TextPost);
    }
};

function validate(title) {
    return /^[A-Z]{1}[a-z0-9\s!:-?,.]{5,60}/.test(title);
}

const setAvatar = (avatar) => {
    return !avatar
        ? incognito
        : avatar.length > 32
            ? `https://secure.gravatar.com/avatar/${avatar.slice(
                -36,
                avatar.length
            )}?s=64`
            : `https://image.tmdb.org/t/p/w64_and_h64_face${avatar}`;
};

searchSwitcher.addEventListener('change', () => {
    localStorage.setItem('switcher', `${searchSwitcher.checked}`);
    if (searchSwitcher.checked) {
        searchInput.setAttribute('placeholder', 'Search by title');
    } else {
        searchInput.setAttribute('placeholder', 'Search by author');
    }
});

searchInput.addEventListener('input', async function() {
    let arr = [];
    let filmsArr = await getFilms();
    let hintsImgArr = new Set();
    let hintsNameArr = new Set();
    
    searchResults.innerHTML = '';

    if (searchSwitcher.checked === false) {
        for (let i=0; i<filmsArr.length-1; i++) {
            let elem = await getData(filmsArr[i]);

            if (elem.username.indexOf(searchInput.value) !== -1 && elem.type === 'review') {
                arr.push(filmsArr[i]);
                hintsImgArr.add(elem.avatar);
                hintsNameArr.add(elem.name);
            }
        }
    } else {
        for (let i=0; i<filmsArr.length-1; i++) {
            let elem = await getData(filmsArr[i]);

            if (validate(elem.originalTitle) === true) {
                if (elem.originalTitle.indexOf(searchInput.value) !== -1) {
                    arr.push(filmsArr[i]);
                    hintsImgArr.add(elem.poster);
                    hintsNameArr.add(elem.filmTitle);
                }
            }
        }
    }

    let names = Array.from(hintsNameArr);
    let imgs = Array.from(hintsImgArr);

    for (let i=0; i<names.length-1; i++) {
        let div = document.createElement('div');
        
        div.classList.add('search__list-item');
        searchResults.append(div);
        
        div.innerHTML = `
            <img 
                class='search__list-img'
                src='${setAvatar(imgs[i])}' />
            ${names[i]}`;
    }

    localStorage.setItem('filter', `${searchInput.value}`);
    
    if (arr.length > 0) {
        return [...postsCollection].forEach((elem, i) => createPosts(elem, i, arr));
    }

    videoArticle.innerText = 'No results';
    textArticle.innerHTML = '';
    melodyArticle.innerHTML = '';
    pictureArticle.innerHTML = '';

});

[...postsCollection].forEach(async (elem, i) => createPosts(elem, i, await getFilms()));