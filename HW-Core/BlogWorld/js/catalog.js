import $ from 'jquery';

import { getFilms, getInfo } from './useApi.js';

const sidebarBlock = document.getElementById('sidebar');
const moviesBlock = document.getElementById('films');
const postsBlock = document.getElementById('posts');
const title = document.getElementById('title');
const message = document.getElementById('content');

function createItems(section, list, arr, key, positionAfter = false, order = 0) {
    let isSidebar = section.id === 'sidebar';
    let suffix = () => isSidebar ? '_s' : '';

    for (let i in arr) {
        let li = document.createElement('li');
        li.classList.add(`${list}-list__item`);
        li.id = `${key}-${i}` + suffix();
        li.textContent = arr[i][key];
        
        positionAfter ? section.children[order].after(li) : section.append(li);
    }
}

function changeActive(target, list) {
    let number = target.id.split('-')[1].split('_')[0];

    $('.' + target.className).not(target).removeClass('active');
    $('#' +target.id).toggleClass('active');

    target.id.split('_').length !== 1 
    ? $('#' + target.id.split('_')[0]).toggleClass('active')
    : $('#' +target.id + '_s').toggleClass('active');

    if (list === 'movies') {
        if (!$('#' +target.id).hasClass('active')) {
            $('.posts-list__item').remove();
            $('.posts-list').text('Choose film');
            return;
        }
    
        mediator.movies[number].choose();
    } else if (list === 'posts') {
        if (!$('#' +target.id).hasClass('active')) {
            $('#title').text('');
            $('#content').text('Empty');
            return;
        }
        
        mediator.posts[number].choose();
    }
}

class Movie {
    constructor(id, title, order, mediator) {
        this.id = id;
        this.title = title;
        this.order = order;
        this.mediator = mediator;
    }

    choose() {
        this.mediator.choosedFilm(this);
    }
}

class Post {
    constructor(author, content, mediator) {
        this.author = author;
        this.content = content;
        this.mediator = mediator;
    }

    choose() {
        this.mediator.choosedPost(this);
    }
}

class MovieSection {
    constructor(elem, sidebar) {
        this.element = elem;
        this.sidebar = sidebar;
    }

    create(arr) {
        this.element.innerHTML = '';
        this.sidebar.innerHTML = '';

        createItems(this.element, 'films', arr, 'title');
        createItems(this.sidebar, 'films', arr, 'title');
    }
}

class PostSection {
    constructor(elem, sidebar) {
        this.element = elem;
        this.sidebar = sidebar;
    }

    update(arr, order) {
        this.element.innerHTML = '';

        let posts = document.querySelectorAll('.posts-list__item');
        posts.forEach(elem => {
            this.sidebar.removeChild(elem);
        });
        
        if (Object.keys(arr).length == 0) {
            let text = 'Nothin found..'
            let li = document.createElement('li');
            li.classList.add('posts-list__item');
            li.textContent = text;

            this.element.innerHTML = text;
            this.sidebar.children[order].after(li);
            return;
        }

        createItems(this.element, 'posts', arr, 'author');
        createItems(this.sidebar, 'posts', arr, 'author', true, order);
    }
}

class ContentSection {
    constructor (title, message) {
        this.title = title;
        this.message = message;
    }

    update(title, text='') {
        this.title.textContent = title
        this.message.textContent = text;
    }
}

class Mediator {
    constructor(posts, movies, content) {
        this.movies = {};
        this.posts = {};
        this.postSection = posts;
        this.movieSection = movies;
        this.contentSection = content;
    }

    setupMovies(arr) {
        let movies = this.movies;
        for (let i=0; i<arr.length; i++) {
            movies[`${i}`] = new Movie(arr[i].id, arr[i].title, i, mediator);
        }
        this.movieSection.create(movies);
    }

    setupPosts(arr, order) {
        let posts = this.posts;
        for (let i in arr) {
            posts[`${i}`] = new Post(arr[i].author, arr[i].content, mediator);
        }
        this.postSection.update(posts, order);
    }

    async choosedFilm(elem) {   
        this.posts = {};
        mediator.setupPosts(await getInfo(elem.id), elem.order);
    }

    choosedPost(elem) {
        this.contentSection.update(elem.author, elem.content);
    }

    click(event) {
        let target = event.target;

        if (target.tagName === 'LI' && target.textContent === 'Nothin found..') {
            return;
        }
        
        if (target.tagName === 'LI' && target.classList.contains('films-list__item')) {
            changeActive(target, 'movies')
        }

        if (target.tagName === 'LI' && target.classList.contains('posts-list__item')) {
            changeActive(target, 'posts')
        }
    }
}

let movieSection = new MovieSection(moviesBlock, sidebarBlock);
let postSection = new PostSection(postsBlock, sidebarBlock);
let contentSection = new ContentSection(title, message);
let mediator = new Mediator(postSection, movieSection, contentSection);

mediator.setupMovies(await getFilms());

window.addEventListener('click', mediator.click);