const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const isDev = process.env.NODE_ENV === 'development';

const filename = ext => isDev 
    ? `[name].${ext}` 
    : `[name].[contenthash].${ext}`;

module.exports = {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        historyApiFallback: true,
        clientLogLevel: 'silent',
        contentBase: './dist',
        open: true,
        disableHostCheck: true,
        compress: true,
        port: 9000,
    },
    entry: {
        app: path.resolve(__dirname, './BlogWorld/js/helpers.js'),
        blog: path.resolve(__dirname, './BlogWorld/js/blog.js'),
        home: path.resolve(__dirname, './BlogWorld/js/home.js'),
        slider: path.resolve(__dirname, './BlogWorld/js/slider.js'),
        catalog: path.resolve(__dirname, './BlogWorld/js/catalog.js'),
        modal: path.resolve(__dirname, './BlogWorld/js/jquery.myModal.js'),
    },
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: `./js/${filename('bundle.js')}`,
        publicPath: 'auto',
    },
    optimization: {
        runtimeChunk: 'single',
        minimize: false,
    },
    plugins: [
        new CleanWebpackPlugin({ cleanStaleWebpackAssets: false }),

        new HtmlWebpackPlugin({
            filename: 'index.html',
            chunks: ['slider', 'app', 'home'],
            template: './BlogWorld/index.html',
            inject: 'body',
            scriptLoading: 'blocking',
            chunksSortMode: 'manual',
        }),
        new HtmlWebpackPlugin({
            filename: 'blog.html',
            chunks: ['app', 'modal', 'blog'],
            template: './BlogWorld/blog.html',
            inject: 'body',
            scriptLoading: 'blocking',
            chunksSortMode: 'manual',
        }),
        new HtmlWebpackPlugin({
            filename: 'catalog.html',
            chunks: ['app', 'catalog'],
            template: './BlogWorld/post-catalog.html',
            inject: 'body',
        }),
        new MiniCssExtractPlugin({
            filename: `./css/${filename('css')}`,
        }),
    ],
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                ],
            },
            {
                test: /\.s[ac]ss/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.html$/,
                loader: 'html-loader',
                options: {
                    esModule: false,
                },
            },
            {
                test: /\.mp3$/i,
                loader: 'file-loader',
                options: {
                    name: `${filename('mp3')}`,
                    outputPath: './assets/audio/',
                },
            },
            {
                test: /\.(eot|ttf|woff|woff2)$/i,
                loader: 'file-loader',
                options: {
                    name: `${filename('[ext]')}`,
                    outputPath: './assets/fonts/',
                },
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                loader: 'file-loader',
                options: {
                    name: `${filename('[ext]')}`,
                    outputPath: './assets/img/',
                },
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|test)$/,
                loader: 'babel-loader',
            },
        ],
    },
    experiments: {
        topLevelAwait: true,
    },
};